# REIGN Junior Back End Developer CHALLENGE 
## Fernando Tolentino

#### IMPORTANT NOTES
* it is important to have postgresql installed, since it is the database to be used.
* to configure the database, it is necessary to have a database with the name postgres, this normally is by default, if it is not this way we must create it.


To start

* create an .env file with the following attributes, these will be the variables that we will use to connect to the database. 

```bash
DB_USER=yourUser
DB_PASSWORD=yourpassword
DB_HOST=localhost
DB_NAME=postgres
```

* Once the .env file has been created

* we will execute the command.
`npm install`

* `npm test`  To run tests.

* `npm start` initialize the server

* import the postman collection file for testing the routes, the file is located in the folder test/postman


Populate the DB for the first time

* It is not necessary to execute any command, the request to the external api will be made automatically and the information will be stored in the database.


Request

* the requests to the routes must be made using postman (the configuration file is in the postman folder).
* The swagger documentation is in the /api-doc path. It can be consulted from any browser.

