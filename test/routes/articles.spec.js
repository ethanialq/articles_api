const session = require("supertest-session");
const { db } = require("../../src/database/db");
const server = require("../../src/server");

const agent = session(server);

describe("Articles routes", () => {
    // afterAll(async () => {
    //     await db.sync({ force: true });
    //     db.close();
    // })
    describe("GET page", () => {
        it("should responds with 200 status", () => agent.get("/api/articles?page=0").expect(200));

        it("should responds an array with 5 items when page is 0", () =>
            agent.get("/api/articles?page=0")
            .then(res => {
                expect(res.body.rows.length).toBe(5);
            })
        );

        it("should responds an array with 5 items when page is not valid", () =>
            agent.get("/api/articles")
            .then(res => {
                expect(res.body.rows.length).toBe(5);
            })
            
        );

        it("should responds with and array and in the first position should be an object with the keys id, title, author, comment, tags", () =>
            agent.get("/api/articles")
            .then(res => {
                expect(res.body.rows[0]).toHaveProperty(
                    "id",
                    "title",
                    "author",
                    "comment",
                    "tags"
                )
            })
        );
    });

    describe('GET filter title', () => {
        it("should responds with 200 status", () => agent.get("/api/articles?page=0&title=Node").expect(200));
        
        it("should return all items if title is not valid", async () => {
            const res1 = await agent.get("/api/articles?page=0&title")
            const res2 = await agent.get("/api/articles?page=0")

            return expect(res1.body.rows[0]).toEqual(res2.body.rows[0])
        })

        it("must return items related to title (example: Node)", () =>
            agent.get("/api/articles?page=0&title=Node")
            .then(res => {
                expect(res.body.rows[0].title).toContain('Node')
            })
        )
    });

    describe('GET filter author', () => {
        it("should responds with 200 status", () => agent.get("/api/articles?page=0&author=Liriel").expect(200));

        it("should return items related to the author (example: Liriel)", () =>
            agent.get("/api/articles?page=0&author=Liriel")
            .then(res => {
                expect(res.body.rows[0].author).toEqual('Liriel')
            })
        )

    });

    describe('GET filter tags', () => {
        it("should responds with 200 status", () => agent.get("/api/articles?page=0&tags=comment").expect(200));

        it("should return items related to the tags", () => 
            agent.get("/api/articles?page=0&tags=comment")
            .then(res => {
                expect(res.body.rows[0].tags).toContain("comment")
            })
        )
    });
});
