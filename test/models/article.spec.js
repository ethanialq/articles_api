const { article, tag, deletedAr, db } = require('../../src/database/db')
const { expect } = require('chai');

describe('Model testing', () => {
    afterAll(async () => {
        await db.sync({ force: true });
        db.close();
    })

    describe('Article Model', () => {
        beforeEach(async () => await article.sync({ force: true }))

        describe('validations', () => {
            it('shoudl be create an article correctly', done => {
                article.create({
                    object_id: 23456,
                    title: "Post de Fernando",
                    author: "Fernando",
                    comment: "Este es un articulo"
                })
                .then(() => done())
                .catch(() => done(new Error('Deberia haberlo creado')))
            })

            it('should throw an error if data is null', done => {
                article.create({})
                .then(() => done(new Error('nombre es requerido')))
                .catch(() => done())
            })
        });
    });

    describe('Tag model', () => {
        beforeEach(async () => await tag.sync({ force: true }))

        describe('validations', () => {
            it('shoudl be create a tag correctly', done => {
                tag.create({
                    name: "tag num 1"
                })
                .then(() => done())
                .catch(() => done(new Error('Deberia haberlo creado')))
            })

            it('should throw an error if data is null', done => {
                tag.create({})
                .then(() => done(new Error('nombre es requerido')))
                .catch(() => done())
            })
        });
    });

    describe('Deleted model', () => {
        beforeEach(async () => await tag.sync({ force: true }))

        describe('validations', () => {
            it('shoudl be create a deletedArticle correctly', done => {
                deletedAr.create({
                    object_id: 231234
                })
                .then(() => done())
                .catch(() => done(new Error('Deberia haberlo creado')))
            })

            it('should throw an error if data is null', done => {
                deletedAr.create({})
                .then(() => done(new Error('nombre es requerido')))
                .catch(() => done())
            })
        });
    });
});