const axios = require('axios').default
const { article, tag, deletedAr, Op } = require('../database/db')

const URL = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs"

const getHackerNews = async () => { 
    try {
        const { data: { hits } } = await axios.get(URL)
        let tagsArray = []

        const articles = hits.map(hit => {
            tagsArray = [...tagsArray, ...hit._tags]; 

            return {
                object_id: hit.objectID,
                title: hit.title || "Sin titulo",
                author: hit.author,
                comment: hit.comment_text || "sin comentarios",
                tags: hit._tags
            }
        })

        await saveTags(tagsArray);

        await saveArticle(articles)

    } catch (error) {
        console.log("function: getHackerNews",error)
    }

}

const saveTags = async arrayTags => {
    try {
        let tagList = await tag.findAll({attributes: ['name']}) // se piden los tags existentes
        tagList = tagList.map(tag => tag.name)
        let simpleArray = [...new Set(arrayTags)] // eliminamos los duplicados

        if(tagList.length) {
            simpleArray = simpleArray.filter(tag => !tagList.includes(tag))
        }

        const result = simpleArray.map(tag => ({name: tag}))
        await tag.bulkCreate(result,{ returning: true})

    } catch (error) {
        return error
    }
}

const saveArticle = async (articleLis) => {
    try {
        const listObjIdsArtcs = await article.findAll({attributes: ['object_id']}) // []                    [1,2,3,4,5]     [1,2,4,5]           [1,2,4]
        const listObjIdsDel = await deletedAr.findAll({attributes: ['object_id']})   // []                    []              [3]                 [3,5]

        let allObjectsIds = [...listObjIdsArtcs, ...listObjIdsDel]                 // []                    [1,2,3,4,5]     [1,2,4,5,3]         [1,2,4,3,5]

        if(allObjectsIds.length) {                                                 // false                  true           true
            allObjectsIds = allObjectsIds.map(obj => obj.object_id)
            articleLis = articleLis.filter(art => !allObjectsIds.includes(parseInt(art.object_id)))//[9,8,2,5,10]     []              []                  [9,8,10]            
        }

        articleLis = articleLis.map (arti => {
            const {tags: TagsArti, ...rest } = arti;

            return article.create({...rest})
            .then(res => tag.findAll({
                attributes: ['topicId'],
                where: {
                    name: {
                        [Op.in]: TagsArti
                    }
                }
                })
                .then(tagsArray => res.addTags(tagsArray))
            ).catch(err => console.log(err))
        })

        Promise.all(articleLis)


    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    getHackerNews
}