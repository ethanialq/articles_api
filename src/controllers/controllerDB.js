const { article, tag, deletedAr, Op } = require('../database/db')

const getAllArticles = async (page, options) => {
    try {
        const clousesoption = { art: {}, tgs: {} }
        if(options.author) clousesoption.art.author = {[Op.iLike]: `%${options.author}%`}
        if(options.title) clousesoption.art.title = {[Op.iLike]: `%${options.title}%`}
        if(options.tags) clousesoption.tgs.name = {[Op.in]: options.tags.split(',')}

        let { rows } = await article.findAndCountAll({
            limit: 5,
            offset: page * 5,
            attributes: {
                exclude: ['updatedAt', 'createdAt']
            },
            where: clousesoption.art,
            order: [
                ['id','ASC']
            ],
            include: [
                {
                    model: tag,
                    where: clousesoption.tgs,
                    attributes: ['name'],
                    through: {
                        attributes: []
                    }
                }
            ]
        })

        rows = rows.map(article => ({
            id: article.id,
            title: article.title,
            author: article.author,
            comment: article.comment,
            tags: article.tags.map(tag => tag.name)
        }))

        return {
            next: rows.length ? `http://localhost:3001/api/articles?page=${page + 1}`: null,
            prev: page ? `http://localhost:3001/api/articles?page=${page - 1}`: null,
            rows
        }
        
    } catch (error) {
        console.log(error)
        return null
    }
}

const delArticle = async id => {
    try {
        const deletedArticle = await article.findByPk(id)

        if(!deletedArticle) throw new Error()

        await deletedAr.create({object_id: deletedArticle.object_id})
        deletedArticle.destroy();
        return deletedArticle;
        

    } catch (error) {
        return null
    }
}



module.exports = {
    getAllArticles,
    delArticle,
}