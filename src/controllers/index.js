const { getAllArticles, delArticle } = require('./controllerDB')

const getArticles = async (req, res) => {
    try {
        let pageBase = 0, { page: myPage, ...rest } = req.query;
        const pageAsNumber = Number.parseInt(myPage);

        if(!Number.isNaN(pageAsNumber) && pageAsNumber > 0) {
            pageBase = pageAsNumber
        }

        const list = await getAllArticles(pageBase, {...rest})

        res.json(list)
    } catch (error) {
        res.status(500).json({msg: "Error"})
    }
}

const delArticles = async (req, res) => {
    try {
        const id = req.params.idarticles;
        if(!id) throw new Error()

        const article = await delArticle(id);
        if(!article) throw new Error() 

        res.json(article)

    } catch (error) {
        res.status(400).json({msg: "No se proporciono un id valido"})
    }
}
module.exports = {
    getArticles,
    delArticles,
}