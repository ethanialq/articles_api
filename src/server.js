const express = require('express')
const morgan = require('morgan')

// swagger 
const swaggerUI = require('swagger-ui-express')
const swaggerSpec = require('./swagger/openapi.json')
const { db } = require('./database/db')
const { getHackerNews } = require('./controllers/controllerAPI')


const server = express()

// Settings
server.set('port', process.env.PORT || 3001)

// middlewares
server.use(express.json())
server.use(express.urlencoded({extended: false}))
server.use(morgan('dev'))

//cors
server.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// routes
server.use('/api-doc', swaggerUI.serve, swaggerUI.setup(swaggerSpec))
server.use('/api/', require('./routes/index'))


const pedidoApi = async () => {
    console.log('hacindo la petición...')
    await getHackerNews()
    // .then(() => console.log('datos guardados'))
    // .catch(err => console.log(err))
}


// inicializando el servidor
db.sync({ force: false }).then(() => {

    pedidoApi()
    const peticion = setInterval(pedidoApi, 3600000)
    server.listen(server.get('port'), () => {
        console.log("servidor corriendo en el puerto",server.get('port'))
    })
})
.catch(err => {
    console.log("erro al inicial", err)
})


module.exports = server
