const { Router } = require('express')
const { getArticles, delArticles } = require('../controllers/index') 

const router = Router()

router.get('/articles', getArticles)

router.delete('/delete/:idarticles', delArticles)



module.exports = router