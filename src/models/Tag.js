const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('tag', {
        topicId: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            defaultValue: undefined,
          },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    })
}