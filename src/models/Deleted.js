const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('deletedAr', {
        object_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
    })
}