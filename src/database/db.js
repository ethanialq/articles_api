require('dotenv').config();
const { Sequelize, Op } = require('sequelize')
const ArticleM = require('../models/Article')
const DeletedAr = require('../models/Deleted')
const TagM = require('../models/Tag')

const {
    DB_USER, DB_PASSWORD, DB_HOST, DB_NAME
} = process.env;

const sequelize = new Sequelize(`postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:5432/${DB_NAME}`, {
    logging: false,
    native: false,
});

ArticleM(sequelize)
TagM(sequelize)
DeletedAr(sequelize)

const { article, tag } = sequelize.models;

article.belongsToMany(tag, { through: 'article_tag'})
tag.belongsToMany(article, { through: 'article_tag'})

module.exports = {
    ...sequelize.models,
    db: sequelize,
    Op
}